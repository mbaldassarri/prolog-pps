% dropAny(?Elem,?List,?OutList)
dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).

% dropFirst: drops only the first occurrence
dropFirst(X,[X|T],T):-!.
dropFirst(X,[H|Xs],[H|L]):- dropFirst(X,Xs,L).

% dropLast: drops only the last occurrence
dropLast(X,[X|T],T).
dropLast(X,[H|Xs],[H|L]):- dropLast(X,Xs,L),!.

% dropAll: drop all occurrences, returning a single list as result
dropAll(X,[X|T],T):-!.
dropAll(X,[H|Xs],[H|L]):- !, dropAll(X,Xs,L).

% fromCircList(+List,-Graph)
fromCircList([H|T], G) :- fromCircList([H|T], G, H).
fromCircList([H1,H2|T],[e(H1,H2)|L], F) :- fromCircList([H2|T],L, F), !.
fromCircList([H|T],[e(H,F)], F).
