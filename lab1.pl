% search(Elem,List)
search(X,[X|_]).
search(X,[_|Xs]) :- search(X,Xs).

% search_two(Elem, List)
% looks for two consecutive occurrences of Elem
% ?- search_two(a,[b,c,a,a,d,e]). -> no
% ?: search_two(a,[b,c,a,d,a,d,e]). -> yes
% NOTE: It's fully relational! Try search_two(X, L) or search_two(X, [b,c,a,d,a,d,e])
search_two(X, [X,Y,X|_]).
search_two(X, [_|Xs]):- search_two(X,Xs).

% search_anytwo(Elem,List)
% looks for any Elem that occurs two times
% ?- search_anytwo(a,[b,c,a,a,d,e]). --> yes 
% ?- search_anytwo(a,[b,c,a,d,e,a,d,e]). --> yes
search_anytwo(X, [X|T]) :- search(X, T).
search_anytwo(X, [_|T]) :- search_anytwo(X, T).


% size(List, Size)
% Size will contain the number of elements in List
% NOTE: It is fully relational. Try ?- size([a,b,c], X)  -> yes, X/3
size([],0).
size([_|T],M) :- size(T,N), M is N+1.

% size(List,Size)
% Size will contain the number of elements in List, 
% written using notation zero, s(zero), s(s(zero))..
size([], zero).
size([_|T], s(N)) :- size(T, N) 

% sum(List,Sum)
sum([], 0).
sum([H|T], SUM) :- sum(T, N), SUM is N + H.

% same(List1,List2)
% are the two lists exactly the same?
% It is fully relational, try ?- same(X,[1,2,3]) or ?- same(X,X)
same([],[]).
same([X|Xs],[X|Ys]) :- same(Xs,Ys).

% all_bigger(List1,List2)
% all elements in List1 are bigger than those in List2, 1 by 1 
% example: all_bigger([10,20,30,40],[9,19,29,39]).
all_bigger([],[]).
all_bigger([H1|T1],[H2|T2]) :- all_bigger(T1, T2), H1 > H2.

% sublist(List1,List2)
% List1 should contain elements all also in List2 
% example: sublist([1,2],[5,3,2,1]).
% NOTE: by trying sublist(X,[1,2,3,4]). all possible (finite) combinations are set in the tree.
sublist([],L2).
sublist([H1|T1], L2) :- sublist(T1,L2), search(H1, L2).

% seqR(N,List)
% list must contain all elements descending
% example: seqR(4,[4,3,2,1,0]).
seqR(N,[]).
seqR(N,[N|T]) :- N>=0, N2 is N-1, seqR(N2,T).

% ---- Optional Exercises ----


% What happens in the tree?
% inv([1|2,3], [3,2,1]):- inv([2,3], [3,2,1], [1|Acc])
% inv([2|3], [3,2,1], Acc):- inv([3], [3,2,1], [2|[1|Acc]])
% inv([3], [3,2,1], [2|Acc]):- inv[[],[3,2,1], [3|[2|[1|Acc]]])
inv([H|T],L) :- inv(T,L,[H|Acc]).
inv([H|T],L,Acc) :- inv(T,L,[H|Acc]).
inv([],L,L).


% Concatenates 1st and 2nd list, with output in 3rd 
append([],L,L).
append([H|T],L,[H|M]):- append(T,L,M).

% double(List,List)
% Taking advantage of append
double([],[]).
double(L1,L2) :- append(L1,L1,L2).

% times(List,N,List)
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).
times(_,0,[]).
times(L1,N,L2) :- N >= 0, N2 is N - 1, times(L1, N2, OUT), append(OUT,L1,L2).

% proj(List,List)
% example: proj([[1,2],[3,4],[5,6]],[1,3,5]).
% The two heads H of the lists must match
proj(_,[]).
proj([[H|_]|T],[H|T2]) :- proj(T,T2).

% Extra
% Takes each element in list and doubles it
% example: ?- doubleElements([1,2,3], [1,1,2,2,3,3]).
doubleElements([],[]).
doubleElements([H|T],[H,H|T1]) :- doubleElements(T,T1).
